﻿using EmployeeRegister.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EmployeeRegister.Persistence.Tests
{
    [TestClass()]
    public class XMLStorageTests
    {
        private static readonly string SERIALIZED_ONLY_ROOT = "<?xml version =\"1.0\" encoding=\"utf-16\"?><ArrayOfEmployee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"></ArrayOfEmployee>";

        private IOAccessor ioAccessor;
        private IEnumerable returned;
        private static List<Employee> sampleEmployees;

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private void GivenInput(string input)
        {
            StringReader stringReader = new StringReader(input);
            Mock<IOAccessor> ioAccessorMock = new Mock<IOAccessor>();
            ioAccessorMock.Setup(x => x.Input()).Returns(stringReader);

            ioAccessor = ioAccessorMock.Object;
        }

        private void GivenOutput()
        {
            StringWriter stringWriter = new StringWriter();
            Mock<IOAccessor> ioAccessorMock = new Mock<IOAccessor>();
            ioAccessorMock.Setup(x => x.Output()).Returns(stringWriter);

            ioAccessor = ioAccessorMock.Object;
        }

        private void GivenSampleEmployees(int number = 1)
        {
            sampleEmployees = new List<Employee>();
            for (int i = 0; i < number; i++)
            {
                Employee employee = new Employee
                {
                    Id = number.ToString(),
                    FirstName = "Employee" + number,
                    LastName = "Sample",
                    BirthDate = new DateTime(1999, 1, number % 28),
                    Position = "Manager" + number,
                    TaxIdentificationNumber = (number * 100).ToString(),
                    Pay = 1000 + number * 100
                };
                sampleEmployees.Add(employee);
            }
        }

        public void WhenRetrieving<T>()
        {
            XMLStorage<T> xmlStorage = new XMLStorage<T>(ioAccessor);
            returned = xmlStorage.Retrieve();
        }

        public void WhenStoring<T>(IEnumerable<T> entities)
        {
            XMLStorage<T> xmlStorage = new XMLStorage<T>(ioAccessor);
            xmlStorage.Store(entities);
        }

        public void ThenShouldRetrieve<T>(IEnumerable<T> expected)
        {
            CollectionAssert.AreEqual(expected.ToList(), ((IEnumerable<T>)returned).ToList());
        }

        public void ThenShouldWrite(string expected)
        {
            Assert.AreEqual(expected, ioAccessor.Output().ToString());
        }

        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ShouldThrowWhenRetrievingInvalidInput()
        {
            GivenInput("INVALID");

            WhenRetrieving<object>();
        }


        [TestMethod()]
        public void ShouldRetrieveEmptyListWhenNoInput()
        {
            GivenInput(string.Empty);

            WhenRetrieving<Employee>();

            ThenShouldRetrieve(Enumerable.Empty<Employee>());
        }

        [TestMethod()]
        public void ShouldRetrieveEmptyListWhenOnlyRoot()
        {
            GivenInput(SERIALIZED_ONLY_ROOT);

            WhenRetrieving<Employee>();

            ThenShouldRetrieve(Enumerable.Empty<Employee>());
        }
    }
}