﻿using EmployeeRegister.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace EmployeeRegister.Interactors.Tests
{
    [TestClass()]
    public class EmployeesInteractorTests
    {
        private Employee CreateSampleEmployee()
        {
            return new Employee
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                BirthDate = new DateTime(1990, 1, 1),
                Position = "Manager",
                TaxIdentificationNumber = "1234567"
            };
        }

        [TestMethod()]
        public void InsertsEmployeeWithUnchangedName()
        {
            Mock<DataSource> dummyDataSource = new Mock<DataSource>();
            Employee employee = CreateSampleEmployee();
            EmployeesInteractor interactor = new EmployeesInteractor(dummyDataSource.Object);

            interactor.AddEmployee(employee);

            dummyDataSource.Verify(ds => ds.AddEmployeeWithGeneratedId(It.Is<Employee>(emp => emp.FirstName == "Jan")), "Did not insert employee with correct name.");

        }
    }
}