﻿$('.datepicker').datepicker({ dateFormat: 'dd.mm.yy'});
$.validator.methods.number = function (value, element) {
    return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)?(?:,\d+)?$/.test(value);
}