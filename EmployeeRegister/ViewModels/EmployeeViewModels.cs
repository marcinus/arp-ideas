﻿using EmployeeRegister.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeeRegister.ViewModels
{
    public class EmployeeViewModel
    {
        [Display(Name = "ID")]
        public string Id { get; set; }
        [Required(ErrorMessage = "Pole Imię jest wymagane")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Pole Nazwisko jest wymagane")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Pole Data Urodzenia jest wymagane")]
        [Display(Name = "Data urodzenia")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }
        [Required(ErrorMessage = "Pole Stanowisko jest wymagane")]
        [Display(Name = "Stanowisko")]
        public string Position { get; set; }
        [Required(ErrorMessage = "Pole NIP jest wymagane")]
        [RegularExpression(@"^(\d{3}-\d{3}-\d{2}-\d{2})|(\d{3}-\d{2}-\d{2}-\d{3})$", ErrorMessage = "Nieprawidłowy NIP")]
        [Display(Name = "NIP")]
        public string TaxIdentificationNumber { get; set; }
        [Required]
        [Display(Name = "Wynagrodzenie [zł]")]
        public decimal Pay { get; set; }

        [Display(Name = "Wiek")]
        public int Age
        {
            get
            {
                var today = DateTime.Today;
                var yearsDifference = today.Year - BirthDate.Year;
                if (BirthDate > today.AddYears(-yearsDifference)) yearsDifference--;
                return yearsDifference;
            }
        }

        [Display(Name = "Imię i nazwisko")]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public Employee ToModel()
        {
            return new Employee
            {
                Id = Id,
                FirstName = FirstName,
                LastName = LastName,
                BirthDate = BirthDate,
                Position = Position,
                TaxIdentificationNumber = TaxIdentificationNumber,
                Pay = Pay
            };
        }

        public static EmployeeViewModel FromModel(Employee employee)
        {
            return new EmployeeViewModel
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                BirthDate = employee.BirthDate,
                Position = employee.Position,
                TaxIdentificationNumber = employee.TaxIdentificationNumber,
                Pay = employee.Pay
            };
        }
    }
}