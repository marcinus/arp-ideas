﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using EmployeeRegister.Models;
using EmployeeRegister.ViewModels;
using EmployeeRegister.Interactors;
using System.Linq;

namespace EmployeeRegister.Controllers
{
    public class EmployeesController : Controller
    {
        private EmployeesInteractor employeeInteractor;

        public EmployeesController(EmployeesInteractor employeeInteractor)
        {
            this.employeeInteractor = employeeInteractor;
        }

        // GET: Employees
        public ActionResult Index(string filterByName, string filterByPosition, string filterByTaxIdentificationNumber)
        {
            List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();
            viewModels = employeeInteractor
                .ListEmployees()
                .Where(x => filterByName == null || (x.FirstName + " " + x.LastName).Contains(filterByName))
                .Where(x => filterByPosition == null || x.Position.Contains(filterByPosition))
                .Where(x => filterByTaxIdentificationNumber == null || x.TaxIdentificationNumber.Contains(filterByTaxIdentificationNumber))
                .Select(x => EmployeeViewModel.FromModel(x)).ToList();
            ViewBag.filterByName = filterByName;
            ViewBag.filterByPosition = filterByPosition;
            ViewBag.filterByTaxIdentificationNumber = filterByTaxIdentificationNumber;
            return View(viewModels);
        }

        // GET: Employees/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeInteractor.FindById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(EmployeeViewModel.FromModel(employee));
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName,BirthDate,Position,TaxIdentificationNumber,Pay")] EmployeeViewModel employeeViewModel)
        {
            if (ModelState.IsValid)
            {
                employeeInteractor.AddEmployee(employeeViewModel.ToModel());
                return RedirectToAction("Index");
            }

            return View(employeeViewModel);
        }

        public ActionResult NewRandom()
        {
            employeeInteractor.AddRandomEmployee();
            return RedirectToAction("Index");
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeInteractor.FindById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(EmployeeViewModel.FromModel(employee));
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,BirthDate,Position,TaxIdentificationNumber,Pay")] EmployeeViewModel employeeViewModel)
        {
            if (ModelState.IsValid)
            {
                employeeInteractor.UpdateEmployee(employeeViewModel.ToModel());
                return RedirectToAction("Index");
            }
            return View(employeeViewModel);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeInteractor.FindById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(EmployeeViewModel.FromModel(employee));
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Employee employee = employeeInteractor.FindById(id);
            employeeInteractor.DeleteEmployee(employee);
            return RedirectToAction("Index");
        }
    }
}
