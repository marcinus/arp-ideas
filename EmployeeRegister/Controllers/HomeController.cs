﻿using EmployeeRegister.Interactors;
using EmployeeRegister.Models;
using System;
using System.Web.Mvc;

namespace EmployeeRegister.Controllers
{
    public class HomeController : Controller
    {
        private EmployeesInteractor employeesInteractor;

        public HomeController(EmployeesInteractor employeesInteractor)
        {
            this.employeesInteractor = employeesInteractor;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}