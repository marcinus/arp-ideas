﻿using System.Web.Mvc;

namespace EmployeeRegister.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View("Error");
        }
    }
}