﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace EmployeeRegister.Persistence
{
    public class XMLStorage<T>
    {
        private IOAccessor ioAccessor;
        private XmlSerializer serializer = new XmlSerializer(typeof(List<T>));

        public XMLStorage(IOAccessor ioAccessor)
        {
            this.ioAccessor = ioAccessor;
        }

        public IEnumerable<T> Retrieve()
        {
            lock (ioAccessor)
            {
                using (TextReader textReader = ioAccessor.Input())
                {
                    if (textReader.Peek() == -1) return Enumerable.Empty<T>();
                    using (XmlReader reader = XmlReader.Create(textReader))
                    {
                        return (IEnumerable<T>)serializer.Deserialize(reader);
                    }
                }
            }
        }

        public void Store(IEnumerable<T> entities)
        {
            lock (ioAccessor)
            {
                using (TextWriter textWriter = ioAccessor.Output())
                {
                    using (XmlWriter writer = XmlWriter.Create(textWriter, new XmlWriterSettings() { Indent = true }))
                    {
                        serializer.Serialize(writer, entities.ToList());
                    }
                }
            }
        }
    }
}