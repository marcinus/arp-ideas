﻿using System;
using EmployeeRegister.Interactors;
using EmployeeRegister.Models;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeRegister.Persistence
{
    public class XMLDataSourceAdapter : DataSource
    {
        private XMLStorage<Employee> xmlDataSource;

        public XMLDataSourceAdapter(XMLStorage<Employee> xmlDataSource)
        {
            this.xmlDataSource = xmlDataSource;
        }

        public Employee FindEmployeeById(string id)
        {
            return FindEmployeeById(id, ListEmployees());
        }

        private Employee FindEmployeeById(string id, IEnumerable<Employee> collection)
        {
            return collection.FirstOrDefault(x => x.Id == id);
        }

        public void AddEmployeeWithGeneratedId(Employee employee)
        {
            lock (xmlDataSource)
            {
                List<Employee> employees = ListEmployees().ToList();
                employee.Id = Guid.NewGuid().ToString();
                employees.Add(employee);
                StoreEmployees(employees);
            }
        }

        public void UpdateEmployee(Employee employee)
        {
            lock (xmlDataSource)
            {
                List<Employee> employees = ListEmployees().ToList();
                Employee toRemove = FindEmployeeById(employee.Id);
                employees.Remove(toRemove);
                employees.Add(employee);
                StoreEmployees(employees);
            }
        }

        public void RemoveEmployee(Employee employee)
        {
            lock (xmlDataSource)
            {
                List<Employee> employees = ListEmployees().ToList();
                employees.Remove(employee);
                StoreEmployees(employees);
            }
        }

        public IEnumerable<Employee> ListEmployees()
        {
            return xmlDataSource.Retrieve();
        }

        private void StoreEmployees(IEnumerable<Employee> employees)
        {
            xmlDataSource.Store(employees);
        }
    }
}