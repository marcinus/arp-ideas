﻿using System.IO;

namespace EmployeeRegister.Persistence
{
    public interface IOAccessor
    {
        TextReader Input();
        TextWriter Output();
    }
}
