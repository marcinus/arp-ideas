﻿using System;
using System.IO;

namespace EmployeeRegister.Persistence
{
    public class FileIOAccessor : IOAccessor
    {
        private string path;

        public FileIOAccessor(string path)
        {
            this.path = path;
        }

        public TextReader Input()
        {
            try
            {
                return File.OpenText(path);
            }
            catch (Exception)
            {
                return TextReader.Null;
            }
        }

        public TextWriter Output()
        {
            return File.CreateText(path);
        }
    }
}