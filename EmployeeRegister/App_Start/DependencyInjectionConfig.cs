﻿using Autofac;
using Autofac.Integration.Mvc;
using EmployeeRegister.Interactors;
using EmployeeRegister.Models;
using EmployeeRegister.Persistence;
using System.Web.Hosting;
using System.Web.Mvc;

namespace EmployeeRegister
{
    public class DependencyInjectionConfig
    {
        private static readonly string EMPLOYEES_XML_PATH = "~/App_Data/employees.xml";

        public static void RegisterServices()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterControllers(typeof(MvcApplication).Assembly);
            IOAccessor employeesFileAccessor = new FileIOAccessor(HostingEnvironment.MapPath(EMPLOYEES_XML_PATH));
            XMLStorage<Employee> employeesStorage = new XMLStorage<Employee>(employeesFileAccessor);
            containerBuilder.RegisterInstance(employeesStorage).As<XMLStorage<Employee>>();
            containerBuilder.RegisterType<XMLDataSourceAdapter>().As<DataSource>().SingleInstance();
            containerBuilder.RegisterType<EmployeesInteractor>().SingleInstance();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(containerBuilder.Build()));
        }
    }
}