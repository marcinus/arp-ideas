﻿using EmployeeRegister.Models;
using System.Collections.Generic;

namespace EmployeeRegister.Interactors
{
    public interface DataSource
    {
        Employee FindEmployeeById(string id);
        void AddEmployeeWithGeneratedId(Employee employee);
        void UpdateEmployee(Employee employee);
        void RemoveEmployee(Employee employee);
        IEnumerable<Employee> ListEmployees();
    }
}
