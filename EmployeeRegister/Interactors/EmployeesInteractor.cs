﻿using EmployeeRegister.Models;
using System.Collections.Generic;

namespace EmployeeRegister.Interactors
{
    public class EmployeesInteractor
    {
        private DataSource dataSource;

        public EmployeesInteractor(DataSource dataSource)
        {
            this.dataSource = dataSource;
        }

        public void AddRandomEmployee()
        {
            Employee employee = EmployeeGenerator.RandomEmployee();
            AddEmployee(employee);
        }

        public Employee FindById(string id)
        {
            return dataSource.FindEmployeeById(id);
        }

        public IEnumerable<Employee> ListEmployees()
        {
            return dataSource.ListEmployees();
        }

        public void AddEmployee(Employee employee)
        {
            dataSource.AddEmployeeWithGeneratedId(employee);
        }

        public void UpdateEmployee(Employee employee)
        {
            dataSource.UpdateEmployee(employee);
        }

        public void DeleteEmployee(Employee employee)
        {
            dataSource.RemoveEmployee(employee);
        }
    }
}