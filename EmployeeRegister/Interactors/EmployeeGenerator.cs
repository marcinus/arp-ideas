﻿using EmployeeRegister.Models;
using System;

namespace EmployeeRegister.Interactors
{
    public class EmployeeGenerator
    {
        private static string[] FirstNames = new string[] { "Marek", "Anna", "Tomasz", "Jan", "Renata", "Jędrzej" };
        private static string[] LastNames = new string[] { "Nowak", "Pawlak", "Kowal", "Środa" };
        private static string[] Positions = new string[] { "Kierownik", "Stażysta", "Młodszy Programista", "Programista", "Starszy programista" };

        public static Employee RandomEmployee()
        {
            Random random = new Random();
            int firstNameId = random.Next(FirstNames.Length);
            int lastNameId = random.Next(LastNames.Length);
            int positionId = random.Next(Positions.Length);
            DateTime start = new DateTime(1940, 1, 1);
            DateTime end = new DateTime(2000, 1, 1);
            int range = (end - start).Days;
            return new Employee
            {
                FirstName = FirstNames[firstNameId],
                LastName = LastNames[lastNameId],
                Position = Positions[positionId],
                Pay = random.Next(10000) + 2000,
                BirthDate = start.AddDays(random.Next(range)),
                TaxIdentificationNumber = string.Format("{0:D3}-{1:D2}-{2:D2}-{3:D3}", random.Next(1000), random.Next(100), random.Next(100), random.Next(1000))
            };
        }
    }
}