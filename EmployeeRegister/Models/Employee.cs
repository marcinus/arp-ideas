﻿using System;

namespace EmployeeRegister.Models
{
    public class Employee
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Position { get; set; }
        public string TaxIdentificationNumber { get; set; }
        public decimal Pay { get; set; }

        public override bool Equals(object other)
        {
            if (other == null) return false;
            if (GetType() != other.GetType()) return false;
            Employee otherEmployee = (Employee)other;
            return (Id == otherEmployee.Id)
                && (FirstName == otherEmployee.FirstName)
                && (LastName == otherEmployee.LastName)
                && (BirthDate.Equals(otherEmployee.BirthDate))
                && (Position == otherEmployee.Position)
                && (TaxIdentificationNumber == otherEmployee.TaxIdentificationNumber)
                && (Pay == otherEmployee.Pay);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}